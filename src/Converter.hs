{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Converter
  ( convertAndWrite
  , runConverter
  , mkCNFState
  , readCNF
  )where

import Control.Monad.State
import Data.Array.Unboxed(elems)
import Language.CNF.Parse.ParseDIMACS(CNF(..), parseFile)

type Literal = Int
type Clause = [Literal]

data CNFState = CNFState
  { _clauses_ :: [Clause]  -- List of clauses
  , _numVars_ :: Int
  , _numClauses_ :: Int
  } deriving Show

mkCNFState :: CNF ->CNFState
mkCNFState cnf = CNFState
  { _clauses_ = [elems cl | cl <- cls ]
  , _numVars_ = nVars
  , _numClauses_ = numClauses cnf
  }
  where nVars = numVars cnf
        cls   = clauses cnf

newtype Converter a = Converter
  { runConverter' :: StateT CNFState IO a
  } deriving (Functor, Applicative, Monad, MonadState CNFState, MonadIO)

getClauses :: Converter [Clause]
getClauses = gets _clauses_

getNumVars :: Converter Int
getNumVars = gets _numVars_

getNumClauses :: Converter Int
getNumClauses = gets _numClauses_


      
readCNF :: String -> IO CNF
readCNF filePath = do
  content <- readFile filePath
  parsedContents <- parseFile filePath
  case parsedContents of
    Left m -> return (CNF 0 0 [])
    Right x -> return x
    
    
-- Write clauses to DIMACS CNF file
writeCNF :: FilePath ->  Converter ()
writeCNF filePath = do
  clauses    <- getClauses
  numClauses <- getNumClauses
  n          <- getNumVars
  liftIO $ writeFile filePath $ unlines $ ("p cnf " ++ show n ++ " " ++ show numClauses) : Prelude.map showClause clauses
  where
    showClause clause = unwords (map show clause) ++ " 0"  


breakClause :: Clause -> Converter [Clause]
breakClause clause
  | length clause <= 3 = return [clause]
  | otherwise = do
      n <- getNumVars
      let (firstTwo, rest) = splitAt 2 clause
          newClause = firstTwo ++ [n+1]
          remainingClause = -(n+1) : rest
      modify $ \st -> st { _numVars_ =  n + 1 }
      remainingClauses <- breakClause remainingClause
      return (newClause : remainingClauses)
      
-- Function to transform a CNF into 3-SAT
convert :: Converter ()
convert = do
  clauses <- getClauses
  transformedClauses <- concat <$> mapM breakClause clauses
  modify $ \st ->
    st { _clauses_ = transformedClauses, _numClauses_ = length transformedClauses }

convertAndWrite :: String -> Converter ()
convertAndWrite path = do
  convert
  writeCNF path

runConverter :: Converter a -> CNFState -> IO (a, CNFState)
runConverter converterComp cnfState = do
  runStateT (runConverter' converterComp) cnfState
