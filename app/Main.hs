module Main where

import System.Environment (getArgs)
import System.FilePath (takeBaseName)
import Converter
  ( convertAndWrite
  , runConverter
  , mkCNFState
  , readCNF)



      
main :: IO ()
main = do
  args <- getArgs
  recHandle args
  where
    recHandle :: [String] -> IO ()
    recHandle [] = return ()
    recHandle (arg:args) = do
      cnf <- readCNF arg
      let state      = mkCNFState cnf
          outputName = takeBaseName arg ++ "-3SAT.cnf" ++ "."
      runConverter (convertAndWrite outputName) state
      putStrLn $ "Done! Converted file name is " ++ outputName
      recHandle args
